# An HTML5 Boilerplate and Bootstrap based template for creating websites.
#
# It includes PHP fucntions useful for BTEC Web and Games students but can also be used for other projects.
#
# Author:     Michael Craddock
# Website:    http://www.reddinomedia.com
# Version:    1.0.1
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# Get the full text of the GPL here: http://www.gnu.org/licenses/gpl.txt
#
# -- CONTRIBUTORS --
#
#   1. Michael Craddock
#   2. Will Hirst
#   3. 
#   4.
#   5. 
#   6.
